export default defineAppConfig({
  title: "Montpellier United FC",
  theme: {
    dark: true,
    colors: {
      primary: "#0062cb",
      secondary: "#edb932",
      black: "rgba(0,0,0,.7)",
    },
  },
});
